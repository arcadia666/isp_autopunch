package com.dx.utils;

import java.text.SimpleDateFormat;

public class DateUtil {
    public static String getFormatDate(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年M月d日");
        return sdf.format(time);
    }
    public static String getYYMMddHHmmssDate(long time){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss ");
        return sdf.format(time);
    }
}
