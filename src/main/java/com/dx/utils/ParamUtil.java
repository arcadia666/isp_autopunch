package com.dx.utils;

import com.dx.pojo.Task;
import org.springframework.util.StringUtils;

/**
 * @program: Isp_Automatic_punch
 * @description:
 * @author: hanabi
 * @create: 2022-08-11 19:34
 **/

public class ParamUtil {
    public static boolean paramCheck(Task task) {
        return !(StringUtils.isEmpty(task.getUsername()) || StringUtils.isEmpty(task.getPassword()) || StringUtils.isEmpty(task.getProvince()) || StringUtils.isEmpty(task.getCity()) || StringUtils.isEmpty(task.getArea()));
    }
}
