package com.dx.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @program: Isp_Automatic_punch
 * @description:
 * @author: hanabi
 * @create: 2022-02-09 21:15
 **/
@EnableTransactionManagement
@Configuration
@MapperScan("com.dx.mapper")
public class MyBatisPlusConfig {
}
