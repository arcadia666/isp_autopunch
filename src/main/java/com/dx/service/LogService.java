package com.dx.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.dx.pojo.LogInfo;

public interface LogService extends IService<LogInfo> {
}
