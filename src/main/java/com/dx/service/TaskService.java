package com.dx.service;

import com.dx.pojo.Task;

import java.io.IOException;
import java.util.List;

public interface TaskService {
    void add(Task task);

    List<Task> selectAll();

    void processTask(Task task) throws IOException;

    void updateByUsername(Task task);

    void resetAll();

    void punchInOneClick();

    Task selectByUsername(Task task);

    boolean removeByUsername(String username) throws Exception;
}
