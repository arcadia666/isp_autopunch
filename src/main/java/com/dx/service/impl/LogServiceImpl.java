package com.dx.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dx.mapper.LogMapper;
import com.dx.pojo.LogInfo;
import com.dx.service.LogService;
import org.springframework.stereotype.Service;

@Service
public class LogServiceImpl extends ServiceImpl<LogMapper, LogInfo> implements LogService {
}
