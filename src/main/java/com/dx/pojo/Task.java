package com.dx.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @program: Isp_Automatic_punch
 * @description:
 * @author: hanabi
 * @create: 2022-02-09 20:47
 **/
@TableName("isp_task_new")
@Data
public class Task {
    @TableId
    private Long id;
    private String username;
    private String password;
    private String province;
    private String city;
    private String area;
    private String addr;
    private String email;
    @TableField("is_success")
    private Integer isSuc;
    @TableField("failed_count")
    private Integer failedCount;
}
