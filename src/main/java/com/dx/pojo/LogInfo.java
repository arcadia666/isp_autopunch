package com.dx.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("isp_logs")
public class LogInfo {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String email;
    private Long date;
    private String info;
}
